# README #

This is a very simple header file that is basically an alias for atomic constructs.
It might seem useless, but in my opinion it is always better to use a cas(pointer,old_val,new_val) instead of using __sync_bool_compare_and_swap(pointer,old_val,new_val)

### Atom.h ###

operations types:
* add
* sub
* or
* and
* xor
* nand
* cas
* vcas
* barrier

for *add*, *sub*, *or*, *and*, *xor* and *nand* operations, two calls are available:
* atom_*name* (ptr, val1, val2)
* *name*_atom (ptr, val1, val2)

The first option executes the operation and returns the previous value held by the pointer.
The other one executes the operation and returns the new value held by the pointer.

* CAS (ptr, val1, val2)
* VCAS (ptr, val1, val2)
The first operation compares the value pointed by the first argument with the second argument, and if they are equal, the pointer is set with the value of the third argument.
If the element pointed by the first argument and the second argument are equal, *CAS* returns true (or 1). By the other hand, *VCAS* returns the previous data held by the pointer.

* barrier()
This operation issues a full memory barrier.